import sqlite3
from sqlite3 import Error


def create_connection(path):
    connection = None
    try:
        connection = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return connection

# --------------------------------------

connection = create_connection("/home/serafimovich/PycharmProjects/serafimovich"
                               "/sql_test_lessions/sql_test3.sqlite")

def execute_read_query(connection, query):
    cursor = connection.cursor()
    result = None
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except Error as e:
        print(f"The error '{e}' occurred")

select_users = "SELECT * from users"

users = execute_read_query(connection, select_users)

for user in users:
    print(user)
