def date_func(date):
    year = date[:4]
    mount = date[4:6]
    day = date[6:]

    if mount == '00' or mount >= '13':
        period = 'ERROR!!! неправильная дата'

    if mount == '01':
        period = day + '-' + 'янв' + '-' + year
    if mount == '02':
        period = day + '-' + 'фев' + '-' + year
    if mount == '03':
        period = day + '-' + 'мар' + '-' + year
    if mount == '04':
        period = day + '-' + 'апр' + '-' + year
    if mount == '05':
        period = day + '-' + 'май' + '-' + year
    if mount == '06':
        period = day + '-' + 'июн' + '-' + year
    if mount == '07':
        period = day + '-' + 'июл' + '-' + year
    if mount == '08':
        period = day + '-' + 'авг' + '-' + year
    if mount == '09':
        period = day + '-' + 'сен' + '-' + year
    if mount == '10':
        period = day + '-' + 'окт' + '-' + year
    if mount == '11':
        period = day + '-' + 'ноя' + '-' + year
    if mount == '12':
        period = day + '-' + 'дек' + '-' + year
    return period


print('Введите дату')
date = input('в формате yyyymmdd: ')
print(date_func(date))
