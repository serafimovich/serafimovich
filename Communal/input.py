mount = ('jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec')

date = input('Введите дату, yyyymmdd: ')

period = date[2:4] + date[4:6]
date_all = date[6:] + '-' + mount[int(date[4:6]) - 1] + '-' + date[:4]

prd = 'p_' + period

pokaz = []

pokaz.append(input('Газ: '))
pokaz.append(input('Свет: '))
pokaz.append(input('Мусор: '))
pokaz.append(input('Интернет: '))
pokaz.append(date_all)

strings = prd + ' ' + '=' + ' ' + str(pokaz) + '\n'

with open('pokaz_base.py', 'a', encoding='utf-8') as file:
    file.write(strings)

contacts_from_sorted = []

file = open('pokaz_base.py', 'r', encoding='utf-8')

for line in file:
    contacts_from_sorted.append(line.strip())

file.close()

magic = sorted(contacts_from_sorted)

with open('pokaz.py', 'w', encoding='utf-8') as output:
    print(*magic, sep='\n', file=output)

print()
print('    ' + '*' * 27)
print('    ' + '* Ваши данные обработаны! *')
print('    ' + '*' * 27)
