# выполнение данного кода создает два
# файла contacts_base.txt и contacts.txt
# в той же директории, в которой находится данный файл

new_contacts = []

name, surname = input('Введите имя: '), input('Введите фамилию: ')
full_name = name + str('_') + surname

new_contacts.append(input('Телефон #1: '))
new_contacts.append(input('Телефон #2: '))
new_contacts.append(input('E-mail: '))
new_contacts.append(input('Адрес: '))
new_contacts.append(input('Дополнительная информация: '))

strings = full_name + ' ' + '=' + ' ' + str(new_contacts) + '\n'

with open('contacts_base.py', 'a', encoding='utf-8') as file:
    file.write(strings)

contacts_from_sorted = []

file = open('contacts_base.py', 'r', encoding='utf-8')

for line in file:
    contacts_from_sorted.append(line.strip())

file.close()

magic = sorted(contacts_from_sorted)

with open('contacts.py', 'w', encoding='utf-8') as output:
    print(*magic, sep='\n', file=output)

print()
print('    ' + '*' * 27)
print('    ' + '* Ваши данные обработаны! *')
print('    ' + '*' * 27)

# -*-*- end coding -*-*-
# encoding with UTF-8
#
# coding by Aleksandr Serafimovich
# tel.: +7-967-670-76-70
