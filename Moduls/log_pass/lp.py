# определим пустой список
places = []

# откроем файл и считаем его содержимое в список
with open('listfile.txt', 'r') as filehandle:
    places = [current_place.rstrip() for current_place in filehandle.readlines()]

places.append(input('city: '))

places_list = list(places)

#  записываем в файл
with open('listfile.txt', 'w') as filehandle:
    filehandle.writelines("%s\n" % place for place in places_list)

print(places_list)
