# программа аутентификации

log_list = ['админ', 'серафимович']
pass_list = ['админ', 'алекс@2703']

# entry or registration
print('Уже есть наш аккаунт?')
answer = input('Yes / No: ')

# entry
if answer in ['YES', 'Yes', 'yes', 'Y', 'y', 'ДА', 'Да', 'да', 'Д', 'д']:
    log_entry = input('login: ')
    if log_entry in log_list:
        i = log_list.index(log_entry, 0, len(log_list))
        pass_entry = input('password: ')
        if pass_entry == pass_list[i]:
            print()
            print('Удачный вход!')
    else:
        print()
        print('Аккаунт не зарегистрирован!')

# registration
elif answer in ['NO', 'No', 'no', 'N', 'n', 'НЕТ', 'Нет', 'нет', 'Н', 'н']:
    log_reg = input('Введите имя: ')
    if log_reg in log_list:
        print()
        print('Имя занято!')
    else:
        pass_reg = input('password: ')
        pass_reg_2 = input('Подтвердите пароль: ')
        if pass_reg != pass_reg_2:
            print()
            print('Пароли не совпадают!')
        elif len(pass_reg) < 8:
            print()
            print('Пароль должен быть не менее 8 символов!!!')
        else:
            log_list.append(log_reg)
            print()
            print('Аккаунт зарегистрирован!')


'''
file = open('log_pass_base.txt', 'r+', encoding='utf-8')

log_load = file.readline()

file.close()

print(log_load)'''

f = open( 'log_pass_base.txt', 'r+' )
f.write( 'log_list' )
f.close()