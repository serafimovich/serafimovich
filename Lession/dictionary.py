# словари (dictionary)

# создание словаря
print('_' * 4, 'создание словаря', '_' * 4)
dict1 = {}
dict2 = dict()
print(type(dict1))
print(type(dict2))
print(dict1)
print(dict2)
print()

# через функцию
dict3 = dict(car = 'машина', apple = 'яблоко', snow = 'снег')

#через фигурные скобки
dict4 = {
'car': 'машина',
'apple': 'яблоко',
'snow': 'снег'
}
print(dict3)
print(dict4)
print()

# изменение словаря
print('_' * 4, 'изменение словаря', '_' * 4)
dict5 = {
'car': 'машина',
'apple': 'яблоко'
}
dict5['orange'] = 'апельсин'   # в квадратных скобках
print(dict5)
print()

# удаление ключа и его об'екта (del)
print('_' * 4, "удаление ключа и его об'екта, del", '_' * 4)
dict6 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
del dict6['apple']
print(dict6)
print()

# перебор элементов словаря (for)
print('_' * 4, 'перебор элементов словаря, for', '_' * 4)
dict7 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
for key in dict7:
	print(key, dict7[key])
print()

# вывод значения по ключу
print('_' * 4, 'вывод значения по ключу', '_' * 4)
dict8 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
print(dict8['apple'])
print()

# методы словарей

# copy() - создание копии словаря
print('_' * 4, 'метод copy()', '_' * 4)
dict9 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
dict10 = dict9.copy()
print(dict10)
print()

# get() - получение значения по ключу
print('_' * 4, 'метод get()', '_' * 4)
dict11 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
print(dict11.get('car'))
print()

# clear() - очистка словаря
print('_' * 4, 'метод clear()', '_' * 4)
dict12 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
dict12.clear()
print(dict12)
print()

# keys() - получение всех ключей словаря
print('_' * 4, 'метод keys()', '_' * 4)
dict13 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
print(dict13.keys())
print()

# values() - получене всех элементов словаря
print('_' * 4, 'метод values()', '_' * 4)
dict14 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
print(dict14.values())
print()

# items() - получение всех элементов словаря, ключи и элементы
print('_' * 4, 'метод items()', '_' * 4)
dict15 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
print(dict15.items())
print()

# pop() - удаляет и возвращает значение ключа
print('_' * 4, 'метод pop()', '_' * 4)
dict16 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
print(dict16.pop('car'))
print()

# popitem() - удаляет и возвращает имя и значение ключа
print('_' * 4, 'метод popitem()', '_' * 4)
dict17 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
print(dict17.popitem())
print(dict17)
print()

# setdefault() - получение значения по ключу. если такого ключа нет, он создается со значением None (если оно не указано в свойствах)
print('_' * 4, 'метод setdefault()', '_' * 4)
dict18 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
print(dict18.setdefault('car'))
print(dict18.setdefault('home', 'дом'))
print(dict18)
print()

# update({}) - обновить значенте по ключам
print('_' * 4, 'метод update({}', '_' * 4)
dict19 = {
'car': 'машина',
'apple': 'яблоко',
'orange': 'апельсин'
}
dict19.update({'car': 'автомобиль', 'home': 'дом'})
print(dict19)
print()
