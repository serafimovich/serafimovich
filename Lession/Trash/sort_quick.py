# быстрая сортировка

def partition(sort_nums, begin, end):
	part = begin
	for i in range(begin+1, end+1):
		if sort_nums[i] <= sort_nums[begin]:
			part += 1
			sort_nums[i], sort_nums[part] = sort_nums[part], sort_nums[i]
	sort_nums[part], sort_nums[begin] = sort_nums[begin], sort_nums[part]
	return part
def quick_sort(sort_nums, begin=0, end=None):
	if end is None:
		end = len(sort_nums) - 1
	def quick(sort_nums, begin, end):
		if begin >= end:
			return
		part = partition(sort_nums, begin, end)
		quick(sort_nums, begin, part-1)
		quick(sort_nums, part+1, end)
	return quick(sort_nums, begin, end)

nums = [54, 43, 3, 11, 0]
quick_sort(nums)
print(nums)

'''Один из самых популярных алгоритмов при сортировке списков. При правильном использовании он не требует много памяти и выполняется очень быстро.

Алгоритм разделяет список на две равные части, принимая псевдослучайный элемент и используя его в качестве опоры, то есть центра деления. Элементы, меньшие, чем опора, перемещаются влево от опоры, а элементы, размер которых больше опоры – вправо. Этот процесс повторяется для списка слева от опоры, а также для массива элементов справа от опоры, пока весь массив не будет отсортирован. Алгоритм быстрой сортировки будет работать медленно, если опорный элемент равен наименьшему или наибольшему элементу списка.'''
