# сортировка слиянием

def mergeSort(sort_nums):
	if len(sort_nums)>1:
		mid = len(sort_nums)//2
		lefthalf = sort_nums[:mid]
		righthalf = sort_nums[mid:]
		mergeSort(lefthalf)
		mergeSort(righthalf)
		i=0
		j=0
		k=0
		while i<len(lefthalf) and j<len(righthalf):
			if lefthalf[i]<righthalf[j]:
				sort_nums[k]=lefthalf[i]
				i=i+1
			else:
				sort_nums[k]=righthalf[j]
				j=j+1
			k=k+1
		while i<len(lefthalf):
			sort_nums[k]=lefthalf[i]
			i=i+1
			k=k+1
		while j<len(righthalf):
			sort_nums[k]=righthalf[j]
			j=j+1
			k=k+1

nums = [54, 43, 3, 11, 0]
mergeSort(nums)
print(nums)

'''Алгоритм разделяет список на две части, каждую из них он разделяет еще на две и так далее, пока не останутся отдельные единичные элементы. Далее соседние элементы сортируются парами. Затем эти пары объединяются и сортируются с другими парами, пока не обработаются все элементы в списке.'''
