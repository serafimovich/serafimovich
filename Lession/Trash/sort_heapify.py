# пирамидальная сортировка

def heapify(sort_nums, heap_size, root):
	l = root
	left = (2 * root) + 1
	right = (2 * root) + 2
	if left < heap_size and sort_nums[left] > sort_nums[l]:
		l = left
	if right < heap_size and sort_nums[right] > sort_nums[l]:
		l = right
	if l != root:
		sort_nums[root], sort_nums[l] = sort_nums[l], sort_nums[root]
		heapify(sort_nums, heap_size, l)
def heap(sort_nums):
					size = len(sort_nums)
					for i in range(size, -1, -1):
						heapify(sort_nums, size, i)
					for i in range(size - 1, 0, -1):
							sort_nums[i], sort_nums[0] = sort_nums[0], sort_nums[i]
							heapify(sort_nums, i, 0)

nums = [54, 43, 3, 11, 0]
heap(nums)
print(nums)

'''Этот алгоритм, как и сортировки вставками или выборкой, делит список на две части. Алгоритм преобразует вторую часть списка в бинарное дерево для эффективного определения самого большого элемента.

Преобразуем список в бинарное дерево, где самый большой элемент является вершиной дерева, и помещаем этот элемент в конец списка. После перестраиваем дерево и помещаем новый наибольший элемент перед последним элементом в списке. Повторяем этот алгоритм, пока все вершины дерева не будут удалены. 

Хоть алгоритм и кажется сложным, он значительно быстрее остальных, что особенно заметно при обработке больших списков.'''
